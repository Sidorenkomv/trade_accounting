package com.kata.trade_accounting;

import com.kata.trade_accounting.dto.OrderDto;
import com.kata.trade_accounting.exception.ModelDeletedException;
import com.kata.trade_accounting.mapper.OrderDtoMapper;
import com.kata.trade_accounting.mapper.OrderMapper;
import com.kata.trade_accounting.model.Warehouse;
import com.kata.trade_accounting.repository.OrderRepository;
import com.kata.trade_accounting.repository.WarehouseRepository;
import com.kata.trade_accounting.service.OrderService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class OrderTests {

    private WarehouseRepository warehouseRepo;
    private OrderRepository orderRepo;
    private OrderService orderService;
    private OrderDtoMapper orderDtoMapper;
    private OrderMapper orderMapper;


    @Autowired
    public OrderTests(WarehouseRepository warehouseRepo, OrderRepository orderRepo, OrderService orderService, OrderMapper orderMapper, OrderDtoMapper orderDtoMapper) {
        this.orderRepo = orderRepo;
        this.warehouseRepo = warehouseRepo;
        this.orderService = orderService;
        this.orderMapper = orderMapper;
        this.orderDtoMapper = orderDtoMapper;
    }


    @Test
    void orderServiceAddRetrieve() {

        // add warehouse and order
        OrderDto expectedDto = new OrderDto();
        expectedDto.setComment("Unique order comment!");
        expectedDto.setOrderType("CUSTOMER");
        expectedDto.setNumber(12345);
        expectedDto.setPaid(false);

        Warehouse warehouseModel = new Warehouse();
        warehouseRepo.saveAndFlush(warehouseModel);
        Long warehouseId = warehouseRepo.findAll().stream().findFirst().get().getId();

        expectedDto.setWarehouseId(warehouseId);

        orderService.add(expectedDto);

        // find order ID through repository
        Long orderId = orderRepo.findAll().stream().filter(order -> order.getNumber() == 12345).findFirst().get().getId();

        // get order through service
        OrderDto returnedDto = orderService.findById(orderId);


        assert (returnedDto.getWarehouseId().equals(expectedDto.getWarehouseId()));
        assert (returnedDto.getComment().equals(expectedDto.getComment()));
        assert (returnedDto.getOrderType().equals(expectedDto.getOrderType()));
        assert (returnedDto.getNumber().equals(expectedDto.getNumber()));
        assert (returnedDto.isPaid() == expectedDto.isPaid());
    }

    @Test
    void orderServiceAddDelete() {
        OrderDto orderDto = new OrderDto();
        orderDto.setNumber(6789);
        orderDto.setOrderType("CUSTOMER");
        orderService.add(orderDto);

        OrderDto returnedDto = orderService.findAll().stream().filter(order -> order.getNumber() == 6789).findFirst().get();

        Assertions.assertThrows(ModelDeletedException.class, () -> orderService.deleteById(returnedDto.getId()));
    }

    @Test
    void orderServiceAddRetrieveUpdate() {
        // add warehouse and order
        Warehouse warehouseModel = new Warehouse();
        warehouseRepo.saveAndFlush(warehouseModel);
        Long warehouseId = warehouseRepo.findAll().stream().findFirst().get().getId();

        OrderDto orderDto = new OrderDto();
        orderDto.setNumber(9999);
        orderDto.setOrderType("SUPPLIER");
        orderDto.setWarehouseId(warehouseId);
        orderDto.setPaid(false);

        orderService.add(orderDto);

        // modify order
        OrderDto returnedDto = orderService.findAll().stream().findFirst().get();

        returnedDto.setPaid(true);
        returnedDto.setComment("Comment");
        returnedDto.setWarehouseId(null);

        orderService.update(returnedDto);

        OrderDto returnedDto2 = orderService.findAll().stream().findFirst().get();


        assert (returnedDto2.isPaid() == returnedDto.isPaid());
        assert (returnedDto2.getComment().equals(returnedDto.getComment()));
        assert (returnedDto2.getWarehouseId() == null);
        assert (returnedDto2.getOrderType().equals(orderDto.getOrderType()));


    }
}
