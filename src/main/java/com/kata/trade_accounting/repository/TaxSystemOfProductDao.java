package com.kata.trade_accounting.repository;

import com.kata.trade_accounting.model.TaxSystemOfProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaxSystemOfProductDao extends JpaRepository<TaxSystemOfProduct, Long> {
}
