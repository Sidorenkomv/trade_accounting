package com.kata.trade_accounting.repository;

import com.kata.trade_accounting.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query("update Order set removed = true where id =?1")
    @Modifying
    int setRemovedFlag(Long id);
}
