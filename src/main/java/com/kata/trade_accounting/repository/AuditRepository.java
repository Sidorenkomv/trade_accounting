package com.kata.trade_accounting.repository;

import com.kata.trade_accounting.model.Audit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface AuditRepository extends JpaRepository<Audit, Long> {
    @Modifying
    @Query("update Audit set removed = true where id = ?1")
    int setRemovedTrue(Long id);
}
