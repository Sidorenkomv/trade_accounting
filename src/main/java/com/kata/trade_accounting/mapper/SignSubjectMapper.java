package com.kata.trade_accounting.mapper;

import com.kata.trade_accounting.dto.SignSubjectDTO;
import com.kata.trade_accounting.model.SignSubject;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class SignSubjectMapper {

    private final ModelMapper mapper;

    public SignSubjectDTO toDto(SignSubject signSubject) {
        return mapper.map(signSubject, SignSubjectDTO.class);
    }

    public SignSubject toEntity(SignSubjectDTO dto) {
        return mapper.map(dto, SignSubject.class);
    }
}
