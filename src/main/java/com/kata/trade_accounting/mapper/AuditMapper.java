package com.kata.trade_accounting.mapper;

import com.kata.trade_accounting.dto.AuditDTO;
import com.kata.trade_accounting.model.Audit;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class AuditMapper {
    private final ModelMapper mapper;

    public AuditMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public AuditDTO toDto(Audit audit) {
        return mapper.map(audit, AuditDTO.class);
    }

    public Audit toEntity(AuditDTO dto) {
        return mapper.map(dto, Audit.class);
    }
}
