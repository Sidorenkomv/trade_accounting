package com.kata.trade_accounting.mapper;

import com.kata.trade_accounting.dto.OrderDto;
import com.kata.trade_accounting.model.Order;
import com.kata.trade_accounting.model.OrderType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.core.convert.converter.Converter;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

@Mapper(componentModel = "spring")
public abstract class OrderMapper implements Converter<OrderDto, Order> {

    @Mappings({
            //TODO: implement commented fields
            //@Mapping(target = "organization", signore = true),        // not implemented
            @Mapping(target = "counterAgent", ignore = true),
            @Mapping(target = "warehouse", ignore = true),
            //@Mapping(target = "contract", ignore = true),             // not implemented
            //@Mapping(target = "project", ignore = true),              // not implemented
            //@Mapping(target = "owner", ignore = true),                // not implemented
            @Mapping(target = "products", ignore = true),
            @Mapping(target = "salesChannel", ignore = true),
            @Mapping(target = "orderType", source = "dto.orderType", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "orderDate", source = "dto.orderDate", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "plannedDate", source = "dto.plannedDate", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    })
    public abstract Order dtoToModel(OrderDto dto);

    public OffsetDateTime mapDate(String dateTime) {
        return OffsetDateTime.parse(dateTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public OrderType mapType(String name) {
        return OrderType.getByLabel(name);
    }

}
