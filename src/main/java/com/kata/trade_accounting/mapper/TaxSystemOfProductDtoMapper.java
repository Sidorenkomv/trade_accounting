package com.kata.trade_accounting.mapper;

import com.kata.trade_accounting.dto.TaxSystemOfProductDto;
import com.kata.trade_accounting.model.TaxSystemOfProduct;
import org.mapstruct.Mapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface TaxSystemOfProductDtoMapper extends Converter<TaxSystemOfProduct, TaxSystemOfProductDto> {

    TaxSystemOfProductDto toTaxSystemOfProductDto(TaxSystemOfProduct taxSystemOfProduct);
}
