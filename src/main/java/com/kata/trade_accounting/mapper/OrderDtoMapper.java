package com.kata.trade_accounting.mapper;

import com.kata.trade_accounting.dto.OrderDto;
import com.kata.trade_accounting.model.Order;
import com.kata.trade_accounting.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.modelmapper.Converter;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Mapper(componentModel = "spring")
public abstract class OrderDtoMapper implements Converter<Order, OrderDto> {

    @Mappings({
            //TODO: implement commented fields
            //@Mapping(target = "organizationId", source = "order.organization"),   // not implemented
            @Mapping(target = "counterAgentId", source = "order.counterAgent.id", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "warehouseId", source = "order.warehouse.id", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            //@Mapping(target = "contractId", source = "order.contract"),           // not implemented
            //@Mapping(target = "projectId", source = "order.project"),             // not implemented
            //@Mapping(target = "ownerId", source = "order.owner"),                 // not implemented
            @Mapping(target = "productsIds", source = "order.products", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "salesChannelId", source = "order.salesChannel.id", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "orderType", source = "order.orderType.label", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "orderDate", source = "order.orderDate", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "plannedDate", source = "order.plannedDate", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),

    })
    public abstract OrderDto modelToDto(Order order);

    abstract List<Long> map(List<Product> products);


    public String mapDateTime(OffsetDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    Long map(Product product) {
        return product.getId();
    }


}
