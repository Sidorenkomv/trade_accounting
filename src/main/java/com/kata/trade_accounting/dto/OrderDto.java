package com.kata.trade_accounting.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Schema(name = "OrderDTO", description = "DTO class corresponding to Order model")
@Data
public class OrderDto {

    // --- Shared fields ---
    private Long id;
    @ApiModelProperty(name = "Order type", allowableValues = "'CUSTOMER' or 'SUPPLIER'")
    private String orderType;

    private Integer number;
    @ApiModelProperty(name = "Order creation date", notes = "Date-time formatted using pattern 2011-12-03T10:15:30+01:00")
    private String orderDate;
    @ApiModelProperty(name = "Planned shipping date", notes = "Date-time formatted using pattern 2011-12-03T10:15:30+01:00")
    private String plannedDate;
    private String comment;
    private String status;

    private Long organizationId;
    private Long counterAgentId;
    private Long warehouseId;
    private Long contractId;
    private Long projectId;
    private Long ownerId;
    private List<Long> productsIds;

    // --- Customer order only fields ---
    private boolean isPaid;
    private boolean accounted; // поле 'Проведено'
    private boolean reserved;
    private String shippingAddress;
    private String addressComment;

    private Long salesChannelId;


    // --- Supplier order only fields ---
    private boolean awaiting;
}
