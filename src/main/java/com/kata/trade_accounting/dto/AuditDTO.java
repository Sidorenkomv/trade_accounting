package com.kata.trade_accounting.dto;

import com.kata.trade_accounting.model.Worker;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "AuditDTO", description = "DTO model of Audit")
public class AuditDTO {
    @Schema(description = "Audit ID", accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    @Schema(description = "Audit Date")
    private Date dateEvent;
    @Schema(description = "Worker")
    private Worker worker;
    @Schema(description = "Event")
    private String event;
}
