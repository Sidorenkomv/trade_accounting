package com.kata.trade_accounting.dto;

import com.kata.trade_accounting.model.SignSubjectType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema
public class SignSubjectDTO {

    private Long id;
    private SignSubjectType type;

}
