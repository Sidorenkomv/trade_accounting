package com.kata.trade_accounting.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaxSystemOfProductDto {

    private  long id;

    @NotBlank(message = "Необходимо указать наименование системы налогообложения")
    private String name;

    private boolean removed;
}
