package com.kata.trade_accounting.controller;

import com.kata.trade_accounting.dto.SignSubjectDTO;
import com.kata.trade_accounting.service.SignSubjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/signSubject")
@Tag(name = "Operation with Sign Subject of Calculation", description = "Basic crud operation with Sign Subject of Calculation")
public class SignSubjectController {

    private final SignSubjectService service;

    @Operation(summary = "Get all Sign Subject of Calculation")
    @Tag(name = "Operation with Sign Subject of Calculation")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Got information about all existing Sign Subject of Calculation",
                            content = {
                                    @Content(
                                            mediaType = "application/json",
                                            array = @ArraySchema(schema = @Schema(implementation = SignSubjectDTO.class)))
                            }),
                    @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Sign Subject of Calculation not found", content = @Content)
            })
    @GetMapping("/")
    public ResponseEntity<List<SignSubjectDTO>> getAll() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Create new Sign Subject of Calculation")
    @Tag(name = "Operation with Sign Subject of Calculation")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Sign Subject of Calculation created",
                            content = {
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = SignSubjectDTO.class))
                            }),
                    @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Sign Subject of Calculation not created", content = @Content)
            })
    @PostMapping(value = "/save", consumes = "application/json")
    public ResponseEntity<SignSubjectDTO> save(
            @Parameter(name = "Sign Subject of Calculation",
                    description = "New Sign Subject of Calculation")
            @RequestBody SignSubjectDTO dto) {
        return new ResponseEntity<>(service.save(dto), HttpStatus.OK);
    }

    @Operation(summary = "Delete Sign Subject of Calculation")
    @Tag(name = "Operation with Sign Subject of Calculation")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Sign Subject of Calculation deleted",
                            content = {
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = SignSubjectDTO.class))
                            }),
                    @ApiResponse(responseCode = "400", description = "Invalid id", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Sign Subject of Calculation not found", content = @Content)
            })
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteById(
            @Parameter(in = ParameterIn.PATH, name = "id",
                    required = true, description = "The identifier of Sign Subject of Calculation",
                    allowReserved = true)
            @PathVariable Long id) {
        service.deleteById(id);
        return new ResponseEntity<>("Sign Subject of Calculation deleted", HttpStatus.OK);
    }

    @Operation(summary = "Get Sign Subject of Calculation by it id")
    @Tag(name = "Operation with Sign Subject of Calculation")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Information about specific Sign Subject of Calculation",
                            content = {
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = SignSubjectDTO.class))
                            }),
                    @ApiResponse(responseCode = "400", description = "Invalid id", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Sign Subject of Calculation not found", content = @Content)
            })
    @GetMapping("/getById/{id}")
    public ResponseEntity<SignSubjectDTO> getById(
            @Parameter(in = ParameterIn.PATH, name = "id",
                    required = true, description = "The identifier of Sign Subject of Calculation",
                    allowReserved = true)
            @PathVariable Long id) {
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

    @Operation(summary = "Edit specific Sign Subject of Calculation")
    @Tag(name = "Operation with Sign Subject of Calculation")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Sign Subject of Calculation edited",
                            content = {
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = SignSubjectDTO.class))
                            }),
                    @ApiResponse(responseCode = "400", description = "Invalid id", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Sign Subject of Calculation not found", content = @Content)
            })
    @PutMapping("/edit/{id}")
    public ResponseEntity<SignSubjectDTO> edit(
            @Parameter(in = ParameterIn.PATH, name = "id",
                    required = true, description = "The identifier of Sign Subject of Calculation",
                    allowReserved = true)
            @PathVariable Long id,
            @Parameter(name = "Changes in Sign Subject of Calculation", description = "Information which must be edited")
            @RequestBody SignSubjectDTO dto) {
        return new ResponseEntity<>(service.edit(id, dto), HttpStatus.OK);
    }
}
