package com.kata.trade_accounting.controller;

import com.kata.trade_accounting.dto.AuditDTO;
import com.kata.trade_accounting.service.AuditService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/audit")
@AllArgsConstructor
@Tag(name = "Audit-Controller", description = "The controller is used to work with the Audit.")
public class AuditController {
    private final AuditService service;

    @Operation(summary = "Get all existing Audit")
    @Tag(name = "Audit-Controller")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Get information about all existing Audit",
                            content = {
                                    @Content(mediaType = "application/json", array = @ArraySchema(
                                            schema = @Schema(implementation = AuditDTO.class)))
                            }),
                    @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Audit not found", content = @Content)
            })
    @GetMapping("/")
    public ResponseEntity<List<AuditDTO>> getAll() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Get Audit by id")
    @Tag(name = "Audit-Controller")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Audit information",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = AuditDTO.class))
                            }),
                    @ApiResponse(responseCode = "400", description = "Invalid id", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Audit not found", content = @Content)
            })
    @GetMapping("/getById/{id}")
    public ResponseEntity<AuditDTO> getById(
            @PathVariable @Parameter(description = "id of Audit") Long id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @Operation(summary = "Add new Audit")
    @Tag(name = "Audit-Controller")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "the Audit has been created",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = AuditDTO.class))
                            }),
                    @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Audit not created", content = @Content)
            })
    @PostMapping("/save")
    public ResponseEntity<AuditDTO> save(
            @RequestBody @Parameter(description = "Audit retention DTO") AuditDTO dto) {
        return new ResponseEntity<>(service.save(dto), HttpStatus.OK);
    }

    @Operation(summary = "Delete Audit")
    @Tag(name = "Audit-Controller")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Audit deleted"),
                    @ApiResponse(responseCode = "400", description = "Invalid id", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Audit not deleted", content = @Content)
            })
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(
            @PathVariable @Parameter(description = "id of Audit") Long id) {
        service.deleteById(id);
        return new ResponseEntity<>("Audit deleted", HttpStatus.OK);
    }

    @Operation(summary = "Edit Audit")
    @Tag(name = "Audit-Controller")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The Audit changed",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = AuditDTO.class))
                            }),
                    @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Audit not changed", content = @Content)
            })
    @PutMapping("/edit/{id}")
    public ResponseEntity<AuditDTO> edit(
            @RequestBody @Parameter(description = "Audit retention DTO") AuditDTO dto) {
        return new ResponseEntity<>(service.update(dto), HttpStatus.OK);
    }
}
