package com.kata.trade_accounting.controller;

import com.kata.trade_accounting.dto.TaxSystemOfProductDto;
import com.kata.trade_accounting.dto.TypeOfProductDto;
import com.kata.trade_accounting.service.TaxSystemOfProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/taxSystemOfProduct")
@Api(tags = "Системы налогообложения")
public class TaxSystemOfProductController {

    private final TaxSystemOfProductService taxSystemOfProductService;


    public TaxSystemOfProductController(TaxSystemOfProductService taxSystemOfProductService) {
        this.taxSystemOfProductService = taxSystemOfProductService;
    }

    @GetMapping()
    @ApiOperation(value = "Получение всех типов систем налогообложения", response = TypeOfProductDto.class, responseContainer = "list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное всех типов систем налогообложения"),
            @ApiResponse(code = 401, message = "Проблема с аутентификацией или авторизацией на сайте"),
            @ApiResponse(code = 403, message = "Недостаточно прав для просмотра контента"),
            @ApiResponse(code = 404, message = "Невозможно найти.")
    })

    public ResponseEntity<List<TaxSystemOfProductDto>> getTaxSystemOfProductDto() {
        return new ResponseEntity<>(taxSystemOfProductService.findAll(), HttpStatus.OK);
    }


    @PostMapping
    @ApiOperation(value = "Добавление новой системы налогообложения", response = TypeOfProductDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное добавление новой системы налогообложения"),
            @ApiResponse(code = 201, message = "Успешное добавление новой системы налогообложения"),
            @ApiResponse(code = 401, message = "Проблема с аутентификацией или авторизацией на сайте"),
            @ApiResponse(code = 403, message = "Недостаточно прав для создания новой системы налогообложения"),
            @ApiResponse(code = 404, message = "Невозможно найти")
    })
    public ResponseEntity<Void> createTaxSystemOfProduct(@RequestBody TaxSystemOfProductDto taxSystemOfProductDto) {
        taxSystemOfProductService.addTaxSystemOfProduct(taxSystemOfProductDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Обновление системы налогообложения")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление системы налогообложения"),
            @ApiResponse(code = 201, message = "Успешное обновление системы налогообложения"),
            @ApiResponse(code = 401, message = "Проблема с аутентификацией или авторизацией на сайте"),
            @ApiResponse(code = 403, message = "Недостаточно прав для обновления системы налогообложения"),
            @ApiResponse(code = 404, message = "Невозможно найти")
    })
    public ResponseEntity<Void> putTaxSystemOfProduct(@RequestBody TaxSystemOfProductDto taxSystemOfProductDto) {
        taxSystemOfProductService.addTaxSystemOfProduct(taxSystemOfProductDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаление системы налогообложения")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Система налогообложения успешно удалена"),
            @ApiResponse(code = 400, message = "По переданному id,  система налогообложения не найдена"),
            @ApiResponse(code = 401, message = "Проблема с аутентификацией или авторизацией на сайте"),
            @ApiResponse(code = 403, message = "Недостаточно прав для удаления системы налогообложения"),
            @ApiResponse(code = 404, message = "Невозможно найти")
    })
    public ResponseEntity<Void> deleteTypeOfProductById(
            @ApiParam(value = "id типа продукции") @PathVariable Long id) {
        taxSystemOfProductService.removeTaxSystemOfProduct(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
