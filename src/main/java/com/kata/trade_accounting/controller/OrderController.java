package com.kata.trade_accounting.controller;

import com.kata.trade_accounting.dto.OrderDto;
import com.kata.trade_accounting.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/order")
@Api(value = "OrderController", description = "Operations related to orders, both customer and supplier", tags = {"order"})
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @Operation(summary = "Add new order")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Added new order"),
            @ApiResponse(responseCode = "404", description = "Could not add: associated objects do not exist")
    })
    @PostMapping("/add")
    public ResponseEntity<OrderDto> addNewOrder(@RequestBody OrderDto orderDto) {
        return new ResponseEntity<>(orderService.add(orderDto), HttpStatus.OK);
    }

    @PostMapping("/add")
    @Operation(summary = "Get an order by it's id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Order found", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order not found or deleted")
    })
    @RequestMapping("/getById/{id}")
    public ResponseEntity<OrderDto> getOrderById(@PathVariable long id) {
        return new ResponseEntity<>(orderService.findById(id), HttpStatus.OK);
    }

    @Operation(summary = "Get all orders")
    @RequestMapping("/getAll")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "List of orders", content = @Content),
    })
    public ResponseEntity<List<OrderDto>> getAllOrders() {
        return new ResponseEntity<>(orderService.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Update an order")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Order deleted", content = @Content),
            @ApiResponse(responseCode = "404", description = "Could not update: order not found or already deleted")
    })
    @PostMapping("/update")
    public ResponseEntity<OrderDto> update(@RequestBody OrderDto orderDto) {
        return new ResponseEntity<>(orderService.update(orderDto), HttpStatus.OK);
    }


    @Operation(summary = "Delete an order")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Order deleted", content = @Content),
            @ApiResponse(responseCode = "404", description = "No such order exists"),
    })
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> removeOrderById(@PathVariable long id) {
        orderService.deleteById(id);
        return new ResponseEntity<>("Order removed", HttpStatus.OK);
    }


}
