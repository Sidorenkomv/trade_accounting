package com.kata.trade_accounting.service;


import com.kata.trade_accounting.dto.SignSubjectDTO;

import java.util.List;

public interface SignSubjectService {

    List<SignSubjectDTO> findAll();

    SignSubjectDTO getById(Long id);

    SignSubjectDTO save(SignSubjectDTO dto);

    void deleteById(Long id);

    SignSubjectDTO edit(Long id, SignSubjectDTO dto);
}
