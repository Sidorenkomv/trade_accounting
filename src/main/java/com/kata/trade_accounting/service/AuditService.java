package com.kata.trade_accounting.service;

import com.kata.trade_accounting.dto.AuditDTO;

import java.util.List;

public interface AuditService {
    List<AuditDTO> findAll();

    AuditDTO findById(Long id);

    AuditDTO save(AuditDTO auditDTO);

    void deleteById(Long id);

    AuditDTO update(AuditDTO auditDTO);
}
