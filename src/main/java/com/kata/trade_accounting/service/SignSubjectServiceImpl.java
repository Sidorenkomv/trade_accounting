package com.kata.trade_accounting.service;

import com.kata.trade_accounting.dto.SignSubjectDTO;
import com.kata.trade_accounting.exception.IdNotFoundException;
import com.kata.trade_accounting.exception.ModelDeletedException;
import com.kata.trade_accounting.mapper.SignSubjectMapper;
import com.kata.trade_accounting.model.SignSubject;
import com.kata.trade_accounting.repository.SignSubjectRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@Service
@AllArgsConstructor
public class SignSubjectServiceImpl implements SignSubjectService {

    private final SignSubjectMapper mapper;

    private final SignSubjectRepository repository;

    @Override
    public List<SignSubjectDTO> findAll() {
        return repository.findAll().stream()
                .filter(Predicate.not(SignSubject::isRemoved))
                .map(mapper::toDto).toList();
    }

    @Override
    public SignSubjectDTO getById(Long id) {
        Optional<SignSubject> signSubject = repository.findById(id);
        if (signSubject.isPresent()) {
            return mapper.toDto(signSubject.get());
        } else {
            throw new IdNotFoundException(String.format("Sign Subject with id = %s not found", id));
        }
    }

    @Override
    public SignSubjectDTO save(SignSubjectDTO dto) {
        SignSubject signSubject = repository.save(mapper.toEntity(dto));
        return mapper.toDto(signSubject);
    }

    @Override
    public void deleteById(Long id) {
        int i = repository.setRemovedTrue(id);
        if (i == 0) {
            throw new IdNotFoundException(String.format("Sign Subject with id = %s not found", id));
        }
    }

    @Override
    public SignSubjectDTO edit(Long id, SignSubjectDTO dto) {
        Optional<SignSubject> signSubject = repository.findById(id);
        if (signSubject.isPresent()) {
            if (signSubject.get().isRemoved()) {
                throw new ModelDeletedException(String.format("Sign Subject with id = %s already deleted", id));
            }
            dto.setId(id);
            return save(dto);
        } else {
            throw new IdNotFoundException(String.format("Sign Subject with id = %s not found", id));
        }
    }
}
