package com.kata.trade_accounting.service;

import com.kata.trade_accounting.dto.TaxSystemOfProductDto;
import com.kata.trade_accounting.exception.NotFoundByIdException;
import com.kata.trade_accounting.mapper.TaxSystemOfProductDtoMapper;
import com.kata.trade_accounting.mapper.TaxSystemOfProductMapper;
import com.kata.trade_accounting.repository.TaxSystemOfProductDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaxSystemOfProductServiceImpl implements TaxSystemOfProductService {

    private final TaxSystemOfProductDao taxSystemOfProductDao;

    private final TaxSystemOfProductMapper taxSystemOfProductMapper;

    private final TaxSystemOfProductDtoMapper taxSystemOfProductDtoMapper;

    public TaxSystemOfProductServiceImpl(TaxSystemOfProductDao taxSystemOfProductDao,
                                         TaxSystemOfProductMapper taxSystemOfProductMapper,
                                         TaxSystemOfProductDtoMapper taxSystemOfProductDtoMapper) {
        this.taxSystemOfProductDao = taxSystemOfProductDao;
        this.taxSystemOfProductMapper = taxSystemOfProductMapper;
        this.taxSystemOfProductDtoMapper = taxSystemOfProductDtoMapper;
    }


    @Override
    public List<TaxSystemOfProductDto> findAll() {
        return taxSystemOfProductDao.findAll().stream()
                .map(taxSystemOfProductDtoMapper::toTaxSystemOfProductDto)
                .toList();
    }


    @Override
    public void addTaxSystemOfProduct(TaxSystemOfProductDto taxSystemOfProductDto) {
        taxSystemOfProductDao.saveAndFlush(taxSystemOfProductMapper.toTaxSystemOfProduct(taxSystemOfProductDto));
    }


    @Override
    public void removeTaxSystemOfProduct(Long id) {
        if (findById(id) != null) {
            findById(id).setRemoved(true);
        } else {
            throw new NotFoundByIdException("Система налогообложения для удаления не найдена");
        }
    }


    @Override
    public TaxSystemOfProductDto findById(Long id) {
        return taxSystemOfProductDtoMapper.toTaxSystemOfProductDto(taxSystemOfProductDao.findById(id).orElse(null));
    }

}
