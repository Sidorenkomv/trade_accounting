package com.kata.trade_accounting.service;

import com.kata.trade_accounting.dto.AuditDTO;
import com.kata.trade_accounting.exception.IdNotFoundException;
import com.kata.trade_accounting.exception.ModelDeletedException;
import com.kata.trade_accounting.mapper.AuditMapper;
import com.kata.trade_accounting.model.Audit;
import com.kata.trade_accounting.repository.AuditRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Predicate;

@Service
@AllArgsConstructor
public class AuditServiceImpl implements AuditService {
    private final AuditRepository auditRepository;
    private final AuditMapper mapper;

    @Override
    public List<AuditDTO> findAll() {
        return auditRepository.findAll().stream()
                .filter(Predicate.not(Audit::isRemoved)).map(mapper::toDto).toList();
    }

    @Override
    public AuditDTO findById(Long id) throws IdNotFoundException {
        Audit audit = auditRepository.findById(id)
                .orElseThrow(() -> new IdNotFoundException("No such Audit with ID " + id));
        if (audit.isRemoved()) {
            throw new IdNotFoundException("Audit was deleted" + id);
        }
        return mapper.toDto(audit);
    }

    @Override
    @Transactional
    public AuditDTO save(AuditDTO auditDTO) {
        Audit audit = auditRepository.findById(auditDTO.getId()).orElse(null);
        if (audit != null) {
            if (audit.isRemoved()) {
                throw new ModelDeletedException("the Audit with this ID" + auditDTO.getId() + " has been deleted");
            } else {
                throw new IdNotFoundException("this ID" + auditDTO.getId() + " is already in use ");
            }
        }
        auditRepository.save(mapper.toEntity(auditDTO));
        return auditDTO;
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        int i = auditRepository.setRemovedTrue(id);
        if (i == 0) {
            throw new IdNotFoundException(String.format("Audit with id=%s not found", id));
        }
    }

    @Override
    @Transactional
    public AuditDTO update(AuditDTO auditDTO) {
        Audit audit = auditRepository.findById(auditDTO.getId())
                .orElseThrow(() -> new IdNotFoundException("No such Audit with ID " + auditDTO.getId()));
        if (audit.isRemoved()) {
            throw new IdNotFoundException("Audit was deleted" + auditDTO.getId());
        }
        auditRepository.save(mapper.toEntity(auditDTO));
        return auditDTO;
    }
}
