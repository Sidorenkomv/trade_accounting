package com.kata.trade_accounting.service;

import com.kata.trade_accounting.dto.TaxSystemOfProductDto;

import java.util.List;

public interface TaxSystemOfProductService {

    List<TaxSystemOfProductDto> findAll();

    void addTaxSystemOfProduct(TaxSystemOfProductDto taxSystemOfProductDto);

    void removeTaxSystemOfProduct(Long id);

    TaxSystemOfProductDto findById(Long id);
}
