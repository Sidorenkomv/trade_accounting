package com.kata.trade_accounting.service;

import com.kata.trade_accounting.dto.OrderDto;
import com.kata.trade_accounting.exception.IdNotFoundException;
import com.kata.trade_accounting.exception.ModelDeletedException;
import com.kata.trade_accounting.mapper.OrderDtoMapper;
import com.kata.trade_accounting.mapper.OrderMapper;
import com.kata.trade_accounting.model.Order;
import com.kata.trade_accounting.model.Product;
import com.kata.trade_accounting.repository.CounterAgentRepository;
import com.kata.trade_accounting.repository.OrderRepository;
import com.kata.trade_accounting.repository.ProductRepository;
import com.kata.trade_accounting.repository.SalesChannelRepository;
import com.kata.trade_accounting.repository.WarehouseRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;
    private final OrderMapper dtoToModelMapper;
    private final OrderDtoMapper modelToDtoMapper;
    private final WarehouseRepository warehouseRepository;
    private final CounterAgentRepository counterAgentRepository;
    private final ProductRepository productRepository;
    private final SalesChannelRepository salesChannelRepository;


    public OrderServiceImpl(OrderRepository repository, OrderMapper dtoToModelMapper, OrderDtoMapper modelToDtoMapper, WarehouseRepository warehouseRepository,
                            CounterAgentRepository counterAgentRepository, ProductRepository productRepository, SalesChannelRepository salesChannelRepository) {
        this.repository = repository;
        this.dtoToModelMapper = dtoToModelMapper;
        this.modelToDtoMapper = modelToDtoMapper;
        this.warehouseRepository = warehouseRepository;
        this.counterAgentRepository = counterAgentRepository;
        this.productRepository = productRepository;
        this.salesChannelRepository = salesChannelRepository;
    }

    @Override
    public OrderDto add(OrderDto dto) {

        Long warehouseId = dto.getWarehouseId();
        Long counteragentId = dto.getCounterAgentId();
        List<Long> productsIds = dto.getProductsIds();
        Long salesChannelId = dto.getSalesChannelId();

        Order newModel = dtoToModelMapper.dtoToModel(dto);

        if (warehouseId != null) {
            newModel.setWarehouse(warehouseRepository.findById(warehouseId).orElseThrow(() -> new IdNotFoundException("No such warehouse")));
        }
        if (counteragentId != null) {
            newModel.setCounterAgent(counterAgentRepository.findById(counteragentId).orElseThrow(() -> new IdNotFoundException("No such counter agent")));
        }

        if (productsIds != null) {
            newModel.setProducts(
                    productsIds
                            .stream()
                            .map(id -> productRepository.findById(id).orElseThrow(() -> new IdNotFoundException("No such product")))
                            .toList()
            );
        }

        if (salesChannelId != null) {
            newModel.setSalesChannel(
                    salesChannelRepository.findById(salesChannelId).orElseThrow(() -> new IdNotFoundException("No such sales channel"))
            );
        }

        return modelToDtoMapper.modelToDto(repository.saveAndFlush(newModel));
    }

    @Override
    public List<OrderDto> findAll() {
        return repository.findAll().stream().map(modelToDtoMapper::modelToDto).toList();
    }

    @Override
    public OrderDto findById(Long id) {
        Order order = repository.findById(id).orElseThrow(() -> new IdNotFoundException("No order with such ID exists"));
        if (order.isRemoved()) {
            throw new ModelDeletedException("This order is deleted");
        } else {
            return modelToDtoMapper.modelToDto(order);
        }
    }

    @Override
    public void deleteById(Long id) {
        if (repository.setRemovedFlag(id) != 0) {
            throw new ModelDeletedException("Couldn't delete: no such order or already deleted");
        }
    }

    @Override
    public OrderDto update(OrderDto orderDto) {
        Order model = dtoToModelMapper.dtoToModel(orderDto);

        Long warehouseId = orderDto.getWarehouseId();
        Long counteragentId = orderDto.getCounterAgentId();
        List<Long> dtoProductIds = orderDto.getProductsIds();
        Long salesChannelId = orderDto.getSalesChannelId();


        if (warehouseId != null) {
            model.setWarehouse(
                    warehouseRepository.findById(warehouseId).orElseThrow(() -> new IdNotFoundException("Couldn't update: no such warehouse"))
            );
        } else {
            model.setWarehouse(null);
        }

        if (counteragentId != null) {
            model.setCounterAgent(
                    counterAgentRepository.findById(counteragentId).orElseThrow(() -> new IdNotFoundException("Couldn't not update: no such counter agent"))
            );
        } else {
            model.setCounterAgent(null);
        }

        if (salesChannelId != null) {
            model.setSalesChannel(
                    salesChannelRepository.findById(salesChannelId).orElseThrow(() -> new IdNotFoundException("No such sales channel"))
            );
        } else {
            model.setSalesChannel(null);
        }


        if (dtoProductIds != null) {

            // if model has null product field
            if (model.getProducts() == null) {
                model.setProducts(
                        dtoProductIds
                                .stream()
                                .map((id) -> productRepository.findById(id).orElseThrow(() -> new IdNotFoundException("No such product")))
                                .collect(Collectors.toList())
                );
            }
            // if model has products
            else {
                List<Product> modelProducts = model.getProducts();

                List<Long> modelProductIds = model.getProducts().stream().map(Product::getId).toList();

                dtoProductIds.forEach((id) -> {
                    if (!modelProductIds.contains(id)) {
                        modelProducts.add(productRepository.findById(id).orElseThrow(() -> new IdNotFoundException("No such product")));
                    }
                });
                modelProductIds.forEach((id) -> {
                    if (!dtoProductIds.contains(id)) {
                        modelProducts.removeIf(product -> Objects.equals(id, product.getId()));
                    }
                });
            }

        }
        return modelToDtoMapper.modelToDto(repository.saveAndFlush(model));
    }
}
