package com.kata.trade_accounting.service;

import com.kata.trade_accounting.dto.OrderDto;

import java.util.List;

public interface OrderService {
    OrderDto add(OrderDto dto);

    List<OrderDto> findAll();

    OrderDto findById(Long id);

    OrderDto update(OrderDto orderDto);

    void deleteById(Long id);

}
