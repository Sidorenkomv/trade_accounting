package com.kata.trade_accounting.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "orders")
public class Order {


    // --- Shared fields ---
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false)
    @ApiModelProperty(required = true)
    private OrderType orderType;
    @Column(columnDefinition = "boolean default false")
    @ApiModelProperty(notes = "False by default")
    private boolean removed;

    private Integer number;
    private OffsetDateTime orderDate;
    private OffsetDateTime plannedDate;
    private String comment;
    private String status;

    @ManyToOne
    private CounterAgent counterAgent;
    @ManyToOne
    private Warehouse warehouse;

    /* TODO: implement | реализовать поля
    private Organization organization;
    private Contract contract;
    private Project project;
    private Owner owner;
     */

    @ManyToMany
    @JoinTable(
            name = "order_product",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    private List<Product> products;


    // --- Customer order only fields ---
    private boolean isPaid;
    private boolean accounted; // поле 'Проведено'
    private boolean reserved;
    private String shippingAddress;
    private String addressComment;

    @ManyToOne
    private SalesChannel salesChannel;

    // --- Supplier order only fields ---
    private boolean awaiting;


}