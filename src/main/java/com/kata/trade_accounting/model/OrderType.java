package com.kata.trade_accounting.model;

public enum OrderType {
    CUSTOMER_ORDER("CUSTOMER"),
    SUPPLIER_ORDER("SUPPLIER");

    public final String label;

    OrderType(String label) {
        this.label = label;
    }

    public static OrderType getByLabel(String label) {
        if (label.equalsIgnoreCase("CUSTOMER")) {
            return CUSTOMER_ORDER;
        } else if (label.equalsIgnoreCase("SUPPLIER")) {
            return SUPPLIER_ORDER;
        } else {
            throw new RuntimeException("No type for this label: " + label);
        }
    }
}
