package com.kata.trade_accounting.model;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Data
@Entity
public class SignSubject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private SignSubjectType type;

    @OneToMany(mappedBy = "signSubject", cascade = CascadeType.ALL)
    private Set<Product> products;

    private boolean removed;
}
