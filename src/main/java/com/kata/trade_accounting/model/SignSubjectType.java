package com.kata.trade_accounting.model;

public enum SignSubjectType {
    PRODUCT,
    EXCISE_PRODUCT,
    COMPOSITE_SUBJECT_CALCULATION,
    OTHER_SUBJECT_CALCULATION
}
